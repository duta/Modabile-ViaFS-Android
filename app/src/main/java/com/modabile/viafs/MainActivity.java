package com.modabile.viafs;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.*;
import com.loopj.android.http.*;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount account;
    private Map<String, Marker[]> markerDictionary = new HashMap<String,Marker[]>();
    private boolean ShowDaftarAfterUpdate;
    private boolean ShowAddAfterUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode("1094955777847-so0d64t2ajnlinlaeeggpuvdfop23urh.apps.googleusercontent.com")
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        UpdateUI();
    }

    private void UpdateUI() {
        account = GoogleSignIn.getLastSignedInAccount(this);
        if (account == null){
            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            findViewById(R.id.imageButtonSignout).setVisibility(View.GONE);
            for(Map.Entry<String, Marker[]> entry : markerDictionary.entrySet()) {
                entry.getValue()[0].remove();
                entry.getValue()[1].remove();
            }
            markerDictionary.clear();
            findViewById(R.id.ImageButtonAddNotif).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.imageButtonSignout).setVisibility(View.VISIBLE);
            FetchShips();
        }
    }

    private void InsertMarker(JSONArray ja) {
        for(Map.Entry<String, Marker[]> entry : markerDictionary.entrySet()) {
            entry.getValue()[0].remove();
            entry.getValue()[1].remove();
        }
        markerDictionary.clear();
        for (int i = 0; i < ja.length(); i++) {
            try {
                JSONObject jo = ja.getJSONObject(i);
                // Add a marker in Sydney and move the camera
                LatLng pos = new LatLng(jo.getDouble("lat"), jo.getDouble("lon"));
                Marker m = mMap.addMarker(new MarkerOptions().position(pos)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.penumpang_static)));
                m.setTag(jo);
                Bitmap bitmap = Bitmap.createBitmap(700, 50, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                TextPaint paint = new TextPaint();

                paint.setColor(Color.BLACK);
                paint.setTextSize(25);
                paint.setStrokeWidth(10);
                canvas.drawText(jo.getString("name"), 370, 25, paint);
                DecimalFormat df2 = new DecimalFormat(".##");
                long dv = Long.valueOf(jo.getString("time"))*1000;// its need to be in milisecond
                    Date df = new java.util.Date(dv);
                    String vv = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(df);
                canvas.drawText(vv+" / " +df2.format(jo.getDouble("speed")) + " K", 370, 50, paint);
                canvas.save();
                Marker m2 = mMap.addMarker(new MarkerOptions().position(pos)
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
                markerDictionary.put(jo.getString("mmsi"), new Marker[] {m, m2});
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (ja.length()!=0){
            findViewById(R.id.ImageButtonAddNotif).setVisibility(View.GONE);
        } else {
            findViewById(R.id.ImageButtonAddNotif).setVisibility(View.VISIBLE);
        }
        if (ShowDaftarAfterUpdate){
            daftarActivity();
            this.ShowDaftarAfterUpdate = false;
        } else if (ShowAddAfterUpdate){
            addActivity();
            this.ShowAddAfterUpdate = false;
        }
    }

    private void FetchShips() {
        String url = "https://viafs.modabile.com/android/list?user_id="+account.getId();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                try {
                    JSONArray testV=new JSONArray(new String(response));
                    InsertMarker(testV);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-3.3, 117.5)));
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.getTag() != null){
            String mmsi = null;
            try {
                mmsi = ((JSONObject)marker.getTag()).getString("mmsi");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            viewActivity(mmsi);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.imageButtonSignout:
                mGoogleSignInClient.signOut()
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                UpdateUI();
                            }
                        });
                break;
            case R.id.ImageButtonAddNotif:
                if (account==null){
                    ShowAddAfterUpdate=true;
                    signIn();
                } else {
                    addActivity();
                }
                break;
            case R.id.ImageButtonAdd:
                if (account==null){
                    ShowAddAfterUpdate=true;
                    signIn();
                } else {
                    addActivity();
                }
                break;
            case R.id.imageButtonMenu:
                if(account==null){
                    ShowDaftarAfterUpdate=true;
                    signIn();
                } else {
                    daftarActivity();
                }
                break;
        }
    }

    public void daftarActivity(){
        Intent myIntent = new Intent(MainActivity.this, DaftarActivity.class);
        String[] names = new String[markerDictionary.size()];
        String[] mmsis = new String[markerDictionary.size()];
        int i = 0;
        for(Map.Entry<String, Marker[]> entry : markerDictionary.entrySet()) {
            try {
                names[i] = ((JSONObject)entry.getValue()[0].getTag()).getString("name");
                mmsis[i] = ((JSONObject)entry.getValue()[0].getTag()).getString("mmsi");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i++;
        }
        myIntent.putExtra("names", names);
        myIntent.putExtra("mmsis", mmsis);
        MainActivity.this.startActivityForResult(myIntent, 1993);
    }

    public void viewActivity(String mmsi) {
        Intent myIntent = new Intent(MainActivity.this, ViewActivity.class);
        myIntent.putExtra("data", ((JSONObject)markerDictionary.get(mmsi)[0].getTag()).toString());
        myIntent.putExtra("id", account.getId());
        MainActivity.this.startActivityForResult(myIntent, 1994);
    }

    public void addActivity() {
        Intent myIntent = new Intent(MainActivity.this, AddActivity.class);
        MainActivity.this.startActivityForResult(myIntent, 1995);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1992);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 1992) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

        if (requestCode == 1993) { //Daftar
            String act = data.getStringExtra("Action");
            if (act.equals("Add")){
                addActivity();
            }
            if (act.equals("View")){
                viewActivity(data.getStringExtra("MMSI"));
            }
        }

        if (requestCode == 1994) { //View
            String act = data.getStringExtra("Action");
            if (act.equals("Daftar")){
                daftarActivity();
            }
            if (act.equals("Delete")){
                UpdateUI();
                this.ShowDaftarAfterUpdate = true;
            }
        }

        if (requestCode == 1995) { //View
            String act = data.getStringExtra("Action");
            if (act.equals("Daftar")){
                daftarActivity();
            }
            if (act.equals("Add")){
                String url = "https://viafs.modabile.com/android/add?user_id="+account.getId()+"&mmsi="+data.getStringExtra("MMSI")+"&name="+data.getStringExtra("name");
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(url, new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        // called before request is started
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                        // called when response HTTP status is "200 OK"

                        UpdateUI();
                        ShowDaftarAfterUpdate = true;
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        // called when request is retried
                    }
                });
            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("-1992", "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
        UpdateUI();
    }
}
