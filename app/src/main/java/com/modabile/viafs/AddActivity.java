package com.modabile.viafs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class AddActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ((SearchView)findViewById(R.id.searchWidget)).setOnClickListener(this);
        ((SearchView)findViewById(R.id.searchWidget)).setOnQueryTextListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent();
        switch (v.getId()) {
            case R.id.searchWidget:
                ((SearchView)findViewById(R.id.searchWidget)).onActionViewExpanded();
                break;
            case R.id.textMap:
                intent.putExtra("Action", "Map");
                setResult(1995, intent);
                this.finish();
                break;
            case R.id.textDaftar:
                intent.putExtra("Action", "Daftar");
                setResult(1995, intent);
                this.finish();
                break;
            default:
                intent.putExtra("Action","Add");
                try {
                    intent.putExtra("MMSI",(String) ((JSONObject)v.getTag()).getString("mmsi"));
                    intent.putExtra("Name",(String) ((JSONObject)v.getTag()).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setResult(1995,intent);
                this.finish();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        search(s);
        return false;
    }

    private void search(String s) {
        ViewGroup viewGroup = findViewById(R.id.layoutAdd);
        viewGroup.removeAllViewsInLayout();
        String url = "http://findship.co/v2_web/ship/search.php?text="+s;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"

                try {
                    JSONObject testV=new JSONObject(new String(response));
                    InsertSearchResult(testV.getJSONArray("ships"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void InsertSearchResult(JSONArray ships) {
        ViewGroup viewGroup = findViewById(R.id.layoutAdd);
        View item = null;
        for (int i = 0; i < ships.length(); i++) {
            try {
                item = LayoutInflater.from(this).inflate(R.layout.item_add, null);
                ((TextView)item.findViewById(R.id.textNameDaftar)).setText(ships.getJSONObject(i).getString("name"));
                ((TextView)item.findViewById(R.id.textMMSIDaftar)).setText(ships.getJSONObject(i).getString("mmsi"));
                item.setOnClickListener(this);
                item.setTag(ships.getJSONObject(i));
                viewGroup.addView(item);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
