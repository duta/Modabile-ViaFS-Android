package com.modabile.viafs;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ViewActivity extends AppCompatActivity implements View.OnClickListener {

    private String user_id;
    private JSONObject data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        Intent intent = getIntent();
        user_id = intent.getStringExtra("id");
        try {
            data = new JSONObject(intent.getStringExtra("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ViewGroup viewGroup = findViewById(R.id.layoutInfo);
        View item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("MMSI");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("mmsi"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Name");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("IMO");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("imo"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Callsign");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("callsign"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Type");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("typedesc"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Length");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("length") + " m");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Width");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("width") + " m");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Depth");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("depth") + " m");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Build");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("build"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Gross ton");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("gt"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("DWT");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("dwt"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Last time");
        try {long dv = Long.valueOf(data.getString("time"))*1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(df);
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(vv);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Status");
        try {
            switch (data.getInt("status")){
                case 0:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Under way using engine");
                    break;
                case 1:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("At anchor");
                    break;
                case 2:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Not under command");
                    break;
                case 3:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Restricted manoeuverability");
                    break;
                case 4:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Constrained by her draught");
                    break;
                case 5:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Moored");
                    break;
                case 6:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Aground");
                    break;
                case 7:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Engaged in Fishing");
                    break;
                case 8:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Under way sailing");
                    break;
                case 9:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Reserved for future amendment of Navigational Status for HSC");
                    break;
                case 10:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Reserved for future amendment of Navigational Status for WIG");
                    break;
                case 11:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Reserved for future use");
                    break;
                case 12:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Reserved for future use");
                    break;
                case 13:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Reserved for future use");
                    break;
                case 14:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("AIS-SART is active");
                    break;
                default:
                    ((TextView)item.findViewById(R.id.textViewDetail)).setText("Not defined (default)");
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Longitude");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("lon"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Latitude");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("lat"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Course");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("course") + " °");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Truehead");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("truehead") + " °");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Speed");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("speed") + " kts");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Draught");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("draft") + " m");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("ETA");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("eta"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
        item = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ((TextView)item.findViewById(R.id.textViewColumn)).setText("Destination");
        try {
            ((TextView)item.findViewById(R.id.textViewDetail)).setText(data.getString("dest"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        viewGroup.addView(item);
    }

    public void onClick(View v) {
        Intent intent=new Intent();
        switch (v.getId()) {
            case R.id.textMap:
                intent.putExtra("Action","Map");
                setResult(1994,intent);
                this.finish();
                break;
            case R.id.textDaftar:
                intent.putExtra("Action","Daftar");
                setResult(1994,intent);
                this.finish();
                break;
            case R.id.ImageButtonDelete:
                new AlertDialog.Builder(this)
                        .setTitle("Hapus Kapal")
                        .setMessage("Lanjutkan menghapus kapal?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                String url = null;
                                try {
                                    url = "https://viafs.modabile.com/android/delete?user_id="+user_id+"&mmsi="+data.getString("mmsi");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                AsyncHttpClient client = new AsyncHttpClient();
                                client.get(url, new AsyncHttpResponseHandler() {

                                    @Override
                                    public void onStart() {
                                        // called before request is started
                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                                        // called when response HTTP status is "200 OK"
                                        Intent intent=new Intent();
                                        intent.putExtra("Action","Delete");
                                        setResult(1994,intent);
                                        finish();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                                    }

                                    @Override
                                    public void onRetry(int retryNo) {
                                        // called when request is retried
                                    }
                                });
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
                break;
        }
    }
}
