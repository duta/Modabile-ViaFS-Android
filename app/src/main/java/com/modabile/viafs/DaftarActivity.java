package com.modabile.viafs;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DaftarActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        Intent intent = getIntent();
        String[] names = intent.getStringArrayExtra("names");
        String[] mmsis = intent.getStringArrayExtra("mmsis");
        ViewGroup viewGroup = findViewById(R.id.layoutDaftar);
        for (int i = 0; i < names.length; i++) {
            View item = LayoutInflater.from(this).inflate(R.layout.item_daftar, null);
            ((TextView)item.findViewById(R.id.textNameDaftar)).setText(names[i]);
            ((TextView)item.findViewById(R.id.textMMSIDaftar)).setText(mmsis[i]);
            item.setOnClickListener(this);
            item.setTag(mmsis[i]);
            viewGroup.addView(item);
        }
    }

    public void onClick(View v) {
        Intent intent=new Intent();
        switch (v.getId()) {
            case R.id.textMap:
                intent.putExtra("Action","Map");
                setResult(1993,intent);
                this.finish();
                break;
            case R.id.ImageButtonAdd:
            case R.id.ImageButtonAddNotif:
                intent.putExtra("Action","Add");
                setResult(1993,intent);
                this.finish();
                break;
            default:
                intent.putExtra("Action","View");
                intent.putExtra("MMSI",(String) v.getTag());
                setResult(1993,intent);
                this.finish();
                break;
        }
    }
}
